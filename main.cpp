// TP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <functional>


struct A {
	int i;
	std::string value;
};

int main() {
	int i = 19283;
	std::string value = "Hello";
	int j = 102309;
	auto lambda = [&](int index) -> void* {
		if (index == 0)
			return &i;
		else if(index == 1)
			return &value;
		return &j;
	};

	std::function<void*(int)> func = lambda;

	std::cout << "struct " << sizeof(A) << std::endl;
	std::cout << "lambda " << sizeof(lambda) << std::endl;
	std::cout << "function " << sizeof(func) << std::endl;
	std::cout << "pointer " << sizeof(void*) << std::endl;

	// Undefined behavior, mais permet de voir comment la mémoire est organisée.
	void** captures = (void**)(&lambda);
	// On accède à la capture de i directement.
	std::cout << *(int*)captures[0] << std::endl;
	// On accède à la capture de value directement.
	std::cout << *(std::string*)captures[1] << std::endl;
	std::cout << *(int*)lambda(0) << std::endl; // i
	std::cout << *(std::string*)lambda(1) << std::endl; // value
	std::cout << *(int*)lambda(2) << std::endl; // j
    return 0;
}

